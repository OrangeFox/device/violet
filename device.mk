# Copyright (C) 2020 The Android Open Source Project
# Copyright (C) 2020 The TWRP Open Source Project
# Copyright (C) 2020 SebaUbuntu's TWRP device tree generator
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Copyright (C) 2019-2024 The OrangeFox Recovery Project
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/base.mk)

LOCAL_PATH := device/xiaomi/violet

# Decryption
PRODUCT_PACKAGES += \
    qcom_decrypt \
    qcom_decrypt_fbe

# Exclude Apex
TW_EXCLUDE_APEX := true

# crypto
TW_INCLUDE_CRYPTO := true
TW_INCLUDE_CRYPTO_FBE := true
TW_INCLUDE_FBE_METADATA_DECRYPT := true
BOARD_USES_METADATA_PARTITION := true
BOARD_USES_QCOM_FBE_DECRYPTION := true
PLATFORM_SECURITY_PATCH := 2127-12-31
PLATFORM_VERSION := 99.87.36
VENDOR_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)
PLATFORM_VERSION_LAST_STABLE := $(PLATFORM_VERSION)

# Recovery
TARGET_RECOVERY_PIXEL_FORMAT := "RGBX_8888"
TARGET_USES_MKE2FS := true
TARGET_RECOVERY_QCOM_RTC_FIX := true

# Libraries
TARGET_RECOVERY_DEVICE_MODULES += \
	libion \
	vendor.display.config@1.0 \
	vendor.display.config@2.0 \
	libdisplayconfig.qti \
    	libicuuc

RECOVERY_LIBRARY_SOURCE_FILES += \
    	$(TARGET_OUT_SHARED_LIBRARIES)/libion.so \
    	$(TARGET_OUT_SYSTEM_EXT_SHARED_LIBRARIES)/vendor.display.config@1.0.so \
    	$(TARGET_OUT_SYSTEM_EXT_SHARED_LIBRARIES)/vendor.display.config@2.0.so \
    	$(TARGET_OUT_SYSTEM_EXT_SHARED_LIBRARIES)/libdisplayconfig.qti.so \
    	$(TARGET_OUT_SHARED_LIBRARIES)/libicuuc.so

# dynamic
ifeq ($(FOX_USE_DYNAMIC_PARTITIONS),1)
  PRODUCT_SHIPPING_API_LEVEL := 28
  PRODUCT_USE_DYNAMIC_PARTITIONS := true
  PRODUCT_RETROFIT_DYNAMIC_PARTITIONS := true

  # keymaster
  OF_DEFAULT_KEYMASTER_VERSION := 4.1

#  TW_SKIP_ADDITIONAL_FSTAB := true
#  TW_FORCE_KEYMASTER_VER := true
  
  # Enable project quotas and casefolding for emulated storage without sdcardfs
  $(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)

  # soong
  PRODUCT_SOONG_NAMESPACES += \
    vendor/qcom/opensource/commonsys-intf/display

  # fastbootd, etc
  PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.0-impl-mock \
    android.hardware.fastboot@1.0-impl-mock.recovery \
    fastbootd

  PRODUCT_PROPERTY_OVERRIDES += \
	ro.fastbootd.available=true \
	ro.boot.dynamic_partitions_retrofit=true \
	ro.boot.dynamic_partitions=true \

  # crypto
  PRODUCT_PROPERTY_OVERRIDES += \
    	ro.crypto.dm_default_key.options_format.version=2 \
    	ro.crypto.volume.filenames_mode=aes-256-cts \
    	ro.crypto.volume.metadata.method=dm-default-key \
    	ro.crypto.volume.options=::v2 \
    	ro.crypto.uses_fs_ioc_add_encryption_key=true
else
  OF_DEFAULT_KEYMASTER_VERSION := 4.0
endif

PRODUCT_PROPERTY_OVERRIDES += \
    	ro.crypto.allow_encrypt_override=true \
    	persist.sys.isUsbOtgEnabled=true \
    	vendor.gatekeeper.disable_spu=true

#  PRODUCT_PROPERTY_OVERRIDES += \
#	keymaster_ver=$(OF_DEFAULT_KEYMASTER_VERSION)

# battery
# use health services for battery
#TW_USE_HEALTH_SERVICES_FOR_BATTERY := true
PRODUCT_PROPERTY_OVERRIDES += \
    ro.charger.enable_suspend=true

# initial prop for variant
ifneq ($(FOX_VARIANT),)
  PRODUCT_PROPERTY_OVERRIDES += \
	ro.orangefox.variant=$(FOX_VARIANT)
endif
#
